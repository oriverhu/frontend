const SUCCESS = 'DATA/SUCCESS';
const ERROR = 'DATA/ERROR';

export const setdata = data => {
    return (
        {
            type: SUCCESS,
            data
        }
    )       
}
export const seterror = error => {
    return (
        {
            type: ERROR,
            error
        }
    )       
}


const initialState = {
    datos: [],
    error: ''
};
//reducer tienen que retornar estados inmutables
export default function(state = initialState, action){
    switch (action.type) {
        case SUCCESS:
            console.log(action.data)
            return {
                datos: action.data,
                error: action.data.length > 0 ? '' : 'Producto no encontrado'
            };
        case ERROR:
            return {
                datos: [],
                error: action.error                
            };;
        default:
            return state
    }  
}