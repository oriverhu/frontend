import React from 'react'
import logo from './../../lider-logo.svg'
import './NavBar.css';

import { connect } from 'react-redux';
import { setdata, seterror } from './../../reducers';

 class NavBar extends React.Component{

    constructor(props){
        super(props);    
        this.state = { 
            search: '',   
        };  
    }
    

    render(){        

        return(
            <nav className="navbar navbar-expand-lg navbar-light bg-light navbarNew"
            >
                <a className="navbar-brand" href="/">
                    <img src={logo} alt="LIDER" style={{
                        height: '5vmin',
                        pointerEvents: 'none'
                    }} />
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                
                
                <form className="col-md-10">                
                    <div className="input-group mb-3">
                        
                            <input type="text" className="form-control inputSearch" placeholder="¿Qué estás Buscando?" aria-label="Recipient's username" aria-describedby="button-addon2" 
                            onChange={(e)=>this.setState({search: e.target.value})}
                            onBlur={()=>this.props.getdata(this.state.search)}
                            />
                              <div class="input-group-append" style={{marginTop: '20px', marginLeft: '10px'}} 
                              >
                              <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-search" fill="white" xmlns="http://www.w3.org/2000/svg"
                              
                              onClick={()=>this.props.getdata(this.state.search)}
                              >
                                <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                                <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                                </svg>

                            </div>

                    </div>
                </form>
                
            </nav>
        )
    }
}

const mapStateToProps = state => {
    return {
      datos: state.data.datos,
    }
  }
  
  const mapDispatchToProps = dispatch => ({
    getdata: (e) => getProducts(e,dispatch),
  })

  const getProducts = async (e,dispatch) =>{
 
       var formBody = [      
        encodeURIComponent("param")+"="+encodeURIComponent(e)
      ]
  
      await fetch("http://localhost:4000/getProductsByParam", {
        method: 'POST',   
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        body: formBody
      }) 
      .then(response =>{
        const statusCode = response.status;
        const data = response.json();
        return Promise.all([statusCode, data]).then(res => ({
          statusCode: res[0],
          data: res[1]
        }));
      })
      .then((res) => {
        const { statusCode, data } = res;
  
        if(statusCode===200){
            dispatch(setdata(data.datos))
        }else{
            dispatch(seterror(data.error))
        }      
  
      })
      .catch(function(err) {
       dispatch(seterror(err))
      });

  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(NavBar);