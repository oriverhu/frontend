import React from 'react'
import { connect } from 'react-redux';

function Products(props){

     const formatNumber=(num)=>{     
        if (num === 'NaN') return '-';
        if (num === 'Infinity') return '&#x221e;';
        num = num.toString().replace(/$|,/g, '');
        if (isNaN(num)) num = "0";
    
        var sign = (num === (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        var cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
            num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
        return "$ "+(((sign) ? '' : '') + num);
     }

    return(

       <div className="row">
        <div className="col-md-12">       

            {(props.error) ? (
                <div className="alert alert-danger" role="alert">
                 { typeof props.error === 'string' ? props.error : 'Error en la consulta' } 
                </div>
            ) : (

                <div className="card-columns">           
                {(props.datos.length>0) && (

                    props.datos.map((ele,i)=>(
                        <div key={i} className="col-md-3">
                            <div  className="card" style={{width: '18rem'}}>
                                <img src={'http://'+ele.image} className="card-img-top" alt={ele.id} />
                                <div className="card-body">
                                    <h5 className="card-title">{ele.id}) {ele.brand}</h5>
                                    <h4>{formatNumber(ele.price)}</h4>
                                    <span style={{color: 'red'}}>{ele.discount > 0 ? 'Descuento: '+ele.discount+'%' : ''}</span>
                                    <p className="card-text">{ele.description}.</p>
                                    <center>
                                        <a href="/" className="btn btn-primary">Agregar</a>
                                    </center>
                                </div>
                            </div>
                        </div>
                    ))
                
                )}                        
                </div>
            
            )}

            </div>
        </div>

    );
}

const mapStateToProps = state => {   
    return {
      datos: state.data.datos,
      error: state.data.error
    }
  }
  
  
  export default connect(mapStateToProps)(Products);