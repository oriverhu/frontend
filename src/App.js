import React from 'react';
import Navbar from './containers/NavBar'
import Products from './containers/Products'


export default class App extends React.Component{
  render(){

    return (
      <div>
        <Navbar 
        />
          <div className="container m-5">           
              <Products 
              />
          </div>
      </div>
    );
  }
}

